let postalCode = document.getElementById('postalCode');
let sublocality = document.getElementById('sublocality');
let locality = document.getElementById('locality');
let administrativeArea = document.getElementById('administrativeArea');
let country = document.getElementById('country');
postalCode.onpaste = function(e){
    e.preventDefault();
}
let baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyC6RxP3jtcY9lMikH4V1GgCwzgT0d2ftzI&address=";
postalCode.addEventListener("input",function(){
    document.getElementById('subcontainer').innerHTML = `<select class="form-control" name="sublocality" id="sublocality"></select>`;
    sublocality = document.getElementById('sublocality');
    locality.innerHTML = "";
    administrativeArea.innerHTML = "";
    country.innerHTML = "";
    let code = postalCode.value;
    let Url = baseUrl+code+"+mx";
    if(code.length==5){
        fetch(Url)
        .then(function(response) {
            return response.json();
        })
        .then(function(response) {
            if(response.results[0]!=null){
                document.getElementById('alert').style.display = "none";
                console.log(response.results[0].formatted_address);
                if(response.results[0].postcode_localities!=null){
                    for(i=0;i<response.results[0].postcode_localities.length;i++){
                        let sub = response.results[0].postcode_localities[i];
                        console.log(sub);
                        sublocality.innerHTML += `<option value="`+sub+`">`+sub+`</option>`;
                    }
                    setLocalities(response.results[0].address_components,0);
                }else{
                    setLocalities(response.results[0].address_components,1);
                }
            } else {
                document.getElementById('alert').style.display = "block";
            }
            
        });
    }
})
function setLocalities(localities,flag){
    if(flag == 0){
        for(i=0;i<localities.length;i++){
            for(j=0;j<localities[i].types.length;j++){
                if(localities[i].types[j] == "locality"){
                    locality.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
                if(localities[i].types[j] == "administrative_area_level_1"){
                    administrativeArea.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
                if(localities[i].types[j] == "country"){
                    country.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
            }
        }
    } else {
        for(i=0;i<localities.length;i++){
            for(j=0;j<localities[i].types.length;j++){
                console.log(localities[i].types[j]);
                if(localities[i].types[j] == "locality"){
                    locality.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
                if(localities[i].types[j] == "administrative_area_level_1"){
                    administrativeArea.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
                if(localities[i].types[j] == "country"){
                    country.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
                if(localities[i].types[j] == "sublocality"){
                    sublocality.innerHTML += `<option value="`+localities[i].long_name+`">`+localities[i].long_name+`</option>`;
                }
            }
        }
    }
    if(sublocality.innerHTML == ""){
        document.getElementById('subcontainer').innerHTML = `<input class="form-control" type="text" name="sublocality" id="sublocality">`;
    }
}
function numericValidate(event,length) {
    if(event.charCode >= 48 && event.charCode <= 57 && length < 5){
      return true;
     }
     return false;        
}